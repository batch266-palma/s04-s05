package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car();
        System.out.println("This car is driven by " + myCar.getDriverName());

        /*Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");

        myPet.speak();

        System.out.print(myPet.getName() + " " + myPet.getBreed() + " " + myPet.getColor());*/

        Dog myDreamPet = new Dog();
        myDreamPet.setName("Tibbers");
        myDreamPet.setColor("Brown");
        myDreamPet.speak();
        System.out.print(myDreamPet.getName() + " " + myDreamPet.getBreed() + " " + myDreamPet.getColor());

        myCar.setName("Toyota");
        myCar.setBrand("Vios");
        myCar.setYearOfMake(2025);

        System.out.println("Car name: " + myCar.getName());
        System.out.println("Car brand: " + myCar.getBrand());
        System.out.println("Car year of make: " + myCar.getYearOfMake());
        System.out.println("Car driver: " + myCar.getDriverName());


    }
}
